/**
 * Created with JetBrains PhpStorm.
 * User: Pablo
 * Date: 24/08/13
 * Time: 16:53
 * To change this template use File | Settings | File Templates.
 */

var dimensions = {w : 0 , h : 0};
var particles = [];
var particleNumber = 0;
var particleSteps = 0;
var particleDisplacement = 0;
var framerate = 0;
var view = null;
var ticker = null;

this.onload = onLoadHandler;

function onLoadHandler(event){
    console.log("[INITIALIZED ]");
    loadConfig();
}

function loadConfig(){
    var xhr = new XMLHttpRequest();
    xhr.open("GET","assets/JSON/config.json",true);
    xhr.onload = onJSONLoadHandler;
    xhr.send();
}

function onJSONLoadHandler(){
    var response = JSON.parse(this.responseText);

    dimensions.w = response["dimensions"]["w"];
    dimensions.h = response["dimensions"]["h"];
    particleNumber = response["particlenumber"];
    particleSteps = response["particlesteps"];
    particleDisplacement = response["particledisplacement"];
    framerate = response["framerate"];

    setup();
    console.log("[CONFIG RETRIEVED]");
}

function setup(){
    for ( var i = 0; i<particleNumber; i++ ){
       particles.push(new Particle(particleSteps,particleDisplacement,dimensions.w,dimensions.h));
    }
    view = new View(dimensions);
    ticker = new Ticker("update()",framerate);
}

function update(){
    //console.log("update");
    for(var i in particles){
        particles[i].step();
    }
    view.clear();
    view.draw(particles);
}