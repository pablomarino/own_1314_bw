/**
 * Created with JetBrains PhpStorm.
 * User: Pablo
 * Date: 24/08/13
 * Time: 22:34
 * To change this template use File | Settings | File Templates.
 */

var body;
var canvas;
var context;
var dimensions;

function View(_dimensions){
    this.dimensions=_dimensions;
    this.body = document.getElementsByTagName("body")[0];

    this.canvas = document.createElement("canvas");
    this.canvas.width=this.dimensions.w;
    this.canvas.height=this.dimensions.h;
    this.context = this.canvas.getContext('2d');
    this.body.appendChild(this.canvas);
}

View.prototype.clear=function(){
    this.context.clearRect(0,0,this.dimensions.w,this.dimensions.h);
    this.context.fillStyle = "gray";
    this.context.fillRect(0,0,this.dimensions.w,this.dimensions.h)
}


View.prototype.draw=function(_particles){

    for(var i = 0; i < _particles.length; i++){
        this.context.beginPath();
        for(var j = 0; j<_particles[i].trail.length; j++){
            if(j!==0){
                this.context.moveTo(_particles[i].trail[j-1].x,_particles[i].trail[j-1].y);
            }
            this.context.lineTo(_particles[i].trail[j].x,_particles[i].trail[j].y);
        }
        this.context.stroke();
    }
}
