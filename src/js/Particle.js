/**
 * Created with JetBrains PhpStorm.
 * User: Pablo
 * Date: 24/08/13
 * Time: 17:59
 * To change this template use File | Settings | File Templates.
 */
var steps;
var displacement;
var trail;
var w;
var h;

function Particle(_steps,_displacement,_width,_height){
    //console.log("[New Particle Created]");

    this.steps = _steps;
    this.displacement = _displacement;
    this.trail = [];
    if(_width){this.w = _width}else{this.w =window.innerWidth};
    if(_height){this.h = _height}else{this.w =window.innerHeight};

    this.__internalfillSteps();
}

Particle.prototype.step=function(){
    // añado un punto
    this.trail.unshift({
        x:this.trail[0].x+this.__internalrandomize(),
        y:this.trail[0].y+this.__internalrandomize()
    });
    // si hay mas puntos que los deseados elimino los que sobren
    this.trail.splice(this.steps,this.trail.length-this.steps);
}

Particle.prototype.__internalfillSteps = function(){
    // Calculo posicion inicial
    var _x=Math.ceil(Math.random()*this.w)
    var _y=Math.ceil(Math.random()*this.h)
    // Añado al array [steps] y genero las coordenadas del trail
   for(var i = 0;i<this.steps;i++){
        this.trail.push({x:_x,y:_y});
        _x += this.__internalrandomize();
        _y += this.__internalrandomize();
    }
}

Particle.prototype.__internalrandomize = function(){
    var d =((Math.random() * this.displacement) - (Math.random() * this.displacement))>>0;
    return d;
}
