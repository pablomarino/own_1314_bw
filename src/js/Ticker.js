/**
 * Created with JetBrains PhpStorm.
 * User: Pablo
 * Date: 24/08/13
 * Time: 23:30
 * To change this template use File | Settings | File Templates.
 */
var framerate=100;
var callback;

function Ticker(_callback,_framerate){
    this.framerate=_framerate;
    this.callback=_callback;
    setInterval(this.callback,this.framerate);
}
