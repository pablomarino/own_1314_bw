# Brownian Walker
**Date:** *2014/08/01.*
**Author:** *Pablo Mariño Boga.*
**Description:** Creates a bunch of randomly moving particles.

**Related readings:**
[The Nature of Code, introduction](http://natureofcode.com/book/introduction/) about random numbers distribution.
[Brownian Motion](https://en.wikipedia.org/wiki/Brownian_motion) Wikipedia.
